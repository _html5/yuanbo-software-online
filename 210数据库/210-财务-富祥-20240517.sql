ALTER TABLE `stos_fin_st`.`proc_entry` 
ADD COLUMN `miscellaneous_ap_code` varchar(20) COMMENT '杂项费费用应付编码' AFTER `ap_fin_code`,
ADD COLUMN `miscellaneous_ap_status` varchar(1) COMMENT '杂项费费用应付状态' AFTER `miscellaneous_ap_code`,
ADD COLUMN `miscellaneous_ap_content` varchar(5000) DEFAULT 'N' COMMENT '杂项费财务返回值' AFTER `miscellaneous_ap_status`;


ALTER TABLE `stos_fin_st`.`proc_entry_detail` 
ADD COLUMN `miscellaneous_supplier_code` varchar(40) COMMENT '杂项费供应商编码' AFTER `project_type_code`;
ADD COLUMN `miscellaneous_supplier_name` varchar(40) COMMENT '杂项费供应商名称' AFTER `miscellaneous_supplier_code`;
ADD COLUMN `miscellaneous_price` decimal(12,4) COMMENT '杂项费单价' AFTER `miscellaneous_supplier_name`;
ADD COLUMN `miscellaneous_amount` decimal(12,4) COMMENT '杂项费总价' AFTER `miscellaneous_price`;



ALTER TABLE `stos_fin_st`.`material_price_bind_detail` 
ADD COLUMN `miscellaneous_supplier_code` varchar(40) COMMENT '杂项费供应商编码' AFTER `pay_supplier_name`;
ADD COLUMN `miscellaneous_supplier_name` varchar(40) COMMENT '杂项费供应商名称' AFTER `miscellaneous_supplier_code`;
ADD COLUMN `miscellaneous_price` decimal(12,4) COMMENT '杂项费单价' AFTER `miscellaneous_supplier_name`;



ALTER TABLE `stos_concreteproduction`.`concr_prod_task` 
ADD COLUMN `mortar_volume` decimal(10, 2) COMMENT '砂浆方量' AFTER `mortar_name`;
