ALTER TABLE `stos_dispatch`.`transport_order` 
ADD COLUMN `retp_tare_weight` decimal(10,2) COMMENT '回程皮重' AFTER `back_tare_weight`,
ADD COLUMN `retp_tare_time` datetime COMMENT '回程时间' default now() AFTER `back_tare_weight`;