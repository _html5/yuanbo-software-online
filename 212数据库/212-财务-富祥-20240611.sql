ALTER TABLE `stos_fin_st`.`sale_return` 
MODIFY COLUMN `sale_outbound_no` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '销售出库单单号' AFTER `sale_return_no`;