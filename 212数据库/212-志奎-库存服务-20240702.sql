ALTER TABLE `stos_warehouse`.`wh_entry_detail` 
ADD COLUMN `other_net_weight` decimal(10, 2) DEFAULT '0.00' COMMENT '对方净重' AFTER `detail_transport_payment_type`;