CREATE TABLE `device_bind_maintenance` (
  `id_` varchar(60) NOT NULL COMMENT 'ID',
  `tenant_id_` varchar(60) DEFAULT '-1' COMMENT '租户id',
  `ref_id_` varchar(60) DEFAULT NULL COMMENT '外键',
  `platform` varchar(60) DEFAULT NULL COMMENT '设备所属平台',
  `is_open` varchar(20) DEFAULT NULL COMMENT '是否开启设备绑定校验(0、关闭 1、开启)',
  `rmk` text COMMENT '备注',
  `create_by_` varchar(60) DEFAULT NULL COMMENT '创建人ID',
  `create_org_id_` varchar(60) DEFAULT NULL COMMENT '创建人组织ID',
  `create_time_` datetime DEFAULT NULL COMMENT '创建时间',
  `creator_name_` varchar(60) DEFAULT NULL COMMENT '创建人名称',
  `update_by_` varchar(60) DEFAULT NULL COMMENT '更新人ID',
  `update_time_` datetime DEFAULT NULL COMMENT '更新时间',
  `updater_name_` varchar(0) DEFAULT NULL COMMENT '更新人名称',
  PRIMARY KEY (`id_`),
  UNIQUE KEY `platform_key` (`platform`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备绑定维护表';
INSERT INTO `stos_device_bind`.`device_bind_maintenance`(`id_`, `tenant_id_`, `ref_id_`, `platform`, `is_open`, `rmk`, `create_by_`, `create_org_id_`, `create_time_`, `creator_name_`, `update_by_`, `update_time_`, `updater_name_`) VALUES ('1807617986966220800', '-1', '0', 'PC', '1', '', '', '', '2024-07-01 11:31:20', '', '', NULL, '');
INSERT INTO `stos_device_bind`.`device_bind_maintenance`(`id_`, `tenant_id_`, `ref_id_`, `platform`, `is_open`, `rmk`, `create_by_`, `create_org_id_`, `create_time_`, `creator_name_`, `update_by_`, `update_time_`, `updater_name_`) VALUES ('1807648549097861120', '-1', '0', 'Mobile', '1', '', '', '', '2024-07-01 13:32:47', '', '', NULL, '');
