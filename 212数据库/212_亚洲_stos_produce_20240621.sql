ALTER TABLE `stos_produce`.`concrete_prod_order_mix_ratio` 
ADD COLUMN `customer_name` varchar(50) COMMENT '客户名称' AFTER `updater_name_`,
ADD COLUMN `customer_code` varchar(100) COMMENT '客户编码' AFTER `customer_name`,
ADD COLUMN `project_name` varchar(200) COMMENT '工程名称' AFTER `customer_code`,
ADD COLUMN `project_code` varchar(40) COMMENT '工程编码' AFTER `project_name`,
ADD COLUMN `project_position_name` varchar(20) COMMENT '施工部位' AFTER `project_code`,
ADD COLUMN `project_position_code` varchar(20) COMMENT '施工部位编码' AFTER `project_position_name`,
ADD COLUMN `strength_level_name` varchar(20) COMMENT '强度等级' AFTER `project_position_code`,
ADD COLUMN `strength_level_code` varchar(20) COMMENT '强度等级编码' AFTER `strength_level_name`,
ADD COLUMN `project_type_name` varchar(20) COMMENT '施工方式' AFTER `strength_level_code`,
ADD COLUMN `project_type_code` varchar(20) COMMENT '施工方式编码' AFTER `project_type_name`,
ADD COLUMN `need_volume` decimal(10, 2) COMMENT '要货数量' AFTER `project_type_code`;