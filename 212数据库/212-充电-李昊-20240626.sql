ALTER TABLE `stos_charging`.`s_charging_station` 
ADD COLUMN `tenant_id_` varchar(20) DEFAULT -1 COMMENT '租户id' AFTER `id`;

ALTER TABLE `stos_charging`.`s_charging_gun` 
ADD COLUMN `tenant_id_` varchar(20) DEFAULT -1 COMMENT '租户id' AFTER `id`;