## 问责流程表单
alter  table stos_bpm.accountability_course modify  audit_signature6    text COMMENT '责任审核6签章';
alter  table stos_bpm.accountability_course modify  audit_signature1   text COMMENT '责任审核1签章';
alter  table stos_bpm.accountability_course modify  audit_signature3   text COMMENT '责任审核3签章';
alter  table stos_bpm.accountability_course modify  audit_signature4   text COMMENT '责任审核4签章';
alter  table stos_bpm.accountability_course modify  audit_signature5   text COMMENT '责任审核5签章';

## 工作指令单
alter  table stos_bpm.workinstructionform modify  Signature_initiator    text COMMENT '发起人签章';
alter  table stos_bpm.workinstructionform modify  Signature_department    text COMMENT '部门签章';
alter  table stos_bpm.workinstructionform modify  Signature_director    text COMMENT '负责人签章';
alter  table stos_bpm.workinstructionform modify  Signature_staff    text COMMENT '员工签章';
alter  table stos_bpm.workinstructionform modify  Initiator_Audit_signature    text COMMENT '发起人审核签章';
alter  table stos_bpm.workinstructionform modify  General_Manager_signature    text COMMENT '总经理审核签章';
alter  table stos_bpm.workinstructionform modify  president_signature    text COMMENT '总经理签章';
alter  table stos_bpm.workinstructionform modify  General_Office_signature    text COMMENT '总经办签章';
alter  table stos_bpm.workinstructionform modify  department_audit_signature    text COMMENT '部门审核签章';

##工作指令单子表
alter  table stos_bpm.work_ins_form_child modify  dep_man_signature    text COMMENT '部门经理签章';
alter  table stos_bpm.work_ins_form_child modify  Signature_staff    text COMMENT '员工签章';