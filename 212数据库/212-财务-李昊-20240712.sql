#bond_pay_refund表增加updater_name_字段
ALTER TABLE `stos_fin_fst`.`bond_pay_refund` 
DROP COLUMN `updater_name_`,
ADD COLUMN `updater_name_` varbinary(60) COMMENT '更新人名称' AFTER `audit_name`;

#油耗新增成本价和总成本
ALTER TABLE `stos_fin_st`.`vehicle_refuel_detail` 
ADD COLUMN `price` varchar(60) COMMENT '成本价' AFTER `audit_name`,
ADD COLUMN `amount` varchar(60) COMMENT '总成本' AFTER `price`;