ALTER TABLE `stos_customer`.`customer_contract_price` 
ADD COLUMN `rebates` decimal(10, 2) COMMENT '回扣' AFTER `estimate_volume`;


ALTER TABLE `stos_sale_order`.`concrete_sale_order` 
ADD COLUMN `temp_risk_limit` DECIMAL(10, 2) COMMENT '临时风险金额' AFTER `min_unit_price`,
ADD COLUMN `temp_freeze_limit` DECIMAL(10, 2) COMMENT '临时冻结金额' AFTER `temp_risk_limit`,
ADD COLUMN `temp_credit_limit` DECIMAL(10, 2) COMMENT '临时信用金额' AFTER `temp_freeze_limit`;
