ALTER TABLE `stos_concreteproduction`.`flag_time` 
ADD COLUMN `consume_flag_id` int(20) COMMENT '从主机获取消耗数据自增GUID标识' AFTER `consume_get_time`,
ADD COLUMN `dosage_flag_id` int(20) COMMENT '从主机辅助表获取消耗数据自增GUID标识' AFTER `consume_flag_id`;
