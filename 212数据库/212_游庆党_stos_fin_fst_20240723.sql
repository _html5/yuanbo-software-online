ALTER TABLE `stos_fin_fst`.`sale_payee` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;
