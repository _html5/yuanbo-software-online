


CREATE TABLE `fm_db_alter` (
  `id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
  `ref_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '外键',
  `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '租户ID',
  `creator_name_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_org_id_` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人组织id',
  `create_by_` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者ID',
  `create_time_` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by_` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time_` datetime DEFAULT NULL COMMENT '更新时间',
   `annex` text DEFAULT NULL COMMENT '附件',
Approval_nodes （200)
user_account    (200)
  `mdfy_reas` text DEFAULT NULL COMMENT '修改原因',
  `mdfy_desc` text DEFAULT NULL COMMENT '修改说明',
  `db_name` varchar(50) DEFAULT NULL COMMENT '数据库名称',
  `db_type_name` varchar(200) DEFAULT NULL COMMENT '修改类型名称',
  `db_type_code` varchar(64) DEFAULT NULL COMMENT '修改类型编码',
  `sql` varchar(500) DEFAULT NULL COMMENT 'SQL语句',

  `crtr_sign` text DEFAULT NULL COMMENT '发起人签章',
  `crtr_sgst` text DEFAULT NULL COMMENT '发起人意见',
  `tech_mgt_sign` text DEFAULT NULL COMMENT '技术负责人签章',
  `tech_mgt_sgst` text DEFAULT NULL COMMENT '技术负责人意见',
  `info_mgt_sign` text DEFAULT NULL COMMENT '信息部负责人签章',
  `info_mgt_sgst` text DEFAULT NULL COMMENT '信息部负责人意见',
  `dept_mgt_sign` text DEFAULT NULL COMMENT '部门经理签章',
  `dept_mgt_sgst` text DEFAULT NULL COMMENT '部门经理签章',
  
  `rmk1` varchar(255) DEFAULT NULL COMMENT '备份字段1',
  `rmk2` varchar(255) DEFAULT NULL COMMENT '备份字段2',
  `rmk3` varchar(255) DEFAULT NULL COMMENT '备份字段3'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci ROW_FORMAT=DYNAMIC COMMENT='数据库修改表单'



  