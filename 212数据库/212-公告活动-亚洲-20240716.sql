CREATE TABLE `stos_notice_activity`.`msg_read` (
  `id_` varchar(20) NOT NULL DEFAULT '' COMMENT 'ID',
  `ref_id_` varchar(20) DEFAULT '' COMMENT '外键',
  `tenant_id_` varchar(20) DEFAULT '-1' COMMENT '租户ID',
  `title` varchar(100) DEFAULT '' COMMENT '标题',
  `business_id` varchar(20) DEFAULT '' COMMENT '业务主键ID',
  `type` varchar(2) DEFAULT '' COMMENT '类型（1公告 2制度 3活动）',
  `receiver` varchar(20) DEFAULT '' COMMENT '接收人',
  `receiver_id` varchar(20) DEFAULT '' COMMENT '接收人ID',
  `receiver_account` varchar(20) DEFAULT '' COMMENT '接收人账号',
  `is_read` varchar(2) DEFAULT '0' COMMENT '是否已读（1是 0否）',
  `read_time` datetime DEFAULT NULL COMMENT '已读时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_by_id_` varchar(24) DEFAULT '' COMMENT '创建人ID',
  `create_org_id_` varchar(24) DEFAULT '' COMMENT '创建人组织ID',
  `create_time_` datetime DEFAULT NULL COMMENT '创建时间',
  `creator_name_` varchar(20) DEFAULT '' COMMENT '创建人名称',
  `update_by_` varchar(24) DEFAULT '' COMMENT '更新人ID',
  `update_time_` datetime DEFAULT NULL COMMENT '更新时间',
  `updater_name_` varchar(20) DEFAULT '' COMMENT '更新人名称',
  PRIMARY KEY (`id_`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='消息阅读表';



ALTER TABLE `stos_notice_activity`.`business_scope` 
ADD COLUMN `org_path_id` VARCHAR(520) COMMENT '组织路径ID' AFTER `business_id`;
