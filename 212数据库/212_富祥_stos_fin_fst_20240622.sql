ALTER TABLE `stos_fin_fst`.`bond_biz_refund` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`other_ar` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`proc_biz_pay` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`other_entry` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `creator_name_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`fee_ap` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态 0草稿  1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`fee_ar` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态  0草稿  1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`agg_prod` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿  1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`agg_simple_receive` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿  1审核中  2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`agg_simple_prod_entry` 
ADD COLUMN `audit_status` varchar(40)  DEFAULT '2'  COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`agg_sale` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2'  COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_name`;

ALTER TABLE `stos_fin_st`.`wra_prod_task` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `entry_fin_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`wra_prod_receive` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_name`;


ALTER TABLE `stos_fin_st`.`wra_prod_entry` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中  2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`wra_sale` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `sale_outbound_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`stocktake_loss` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`stocktake_profit` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_st`.`concrete_prod_task` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `entry_fin_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`concrete_prod_receive` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `creator_name_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`concrete_prod_entry` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `creator_name_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_st`.`concre_sale_outbound` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `sale_outbound_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`concre_sale_transport` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' ,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`allot` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿  1审核中  2已审核' ,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`allot_transport` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `vehicle_license_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`proc_return_transport` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `remark`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_st`.`vehicle_charge_detail` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿  1审核中  2已审核' AFTER `remark`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_st`.`vehicle_refuel_detail` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `remark`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`sale_outbound` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿  1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`prod_return_mtrl` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '2' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_st`.`sale_return` 
MODIFY COLUMN `audit_status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '2' COMMENT '审核状态(0草稿，1审核中,2已审核)' AFTER `ar_status`; 

ALTER TABLE `stos_fin_st`.`proc_return` 
MODIFY COLUMN `audit_status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '2' COMMENT '审核状态(0未审核，1审核中,2已审核)' AFTER `ap_no_status`;

ALTER TABLE `stos_fin_st`.`proc_entry` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '2' COMMENT '审核状态 0草稿  1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;



ALTER TABLE `stos_fin_fst`.`other_payment_entry` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态   0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`payee_refund` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`bond_pay_refund` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中  2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`proc_pay_refund` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_fst`.`other_biz_pay` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_name`;

ALTER TABLE `stos_fin_fst`.`bond_pay` 
ADD COLUMN `audit_status` varchar(40) DEFAULT '0' COMMENT '审核状态  0草稿  1审核中  2已审核' AFTER `document_number`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_status`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_name`;


ALTER TABLE `stos_fin_fst`.`other_biz_payee` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`bond_payee` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`asset_ap` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`asset_disp` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`asset_card` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中  2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`cost_adj` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `remark`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`other_outbound` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`bank_transfer` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态  0草稿  1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`asset_change` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态  0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`other_biz_refund` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_fst`.`other_pay_refund` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `document_number`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_fst`.`fa_adjust` 
ADD COLUMN `audit_status` varchar(1) DEFAULT '0' COMMENT '审核状态 0草稿 1审核中 2已审核' AFTER `update_by_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;


ALTER TABLE `stos_fin_st`.`proc_transport` 
MODIFY COLUMN `vehicle_mode` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '车辆能源类型(1油车，2电车)' AFTER `vehicle_type`;

ALTER TABLE `stos_fin_st`.`concrete_prod_receive` 
MODIFY COLUMN `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '-1' COMMENT '租户ID' AFTER `ref_id_`;

ALTER TABLE `stos_fin_st`.`concrete_prod_entry` 
MODIFY COLUMN `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '-1' COMMENT '租户ID' AFTER `ref_id_`;

ALTER TABLE `stos_fin_st`.`concre_sale_outbound` 
MODIFY COLUMN `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '-1' COMMENT '租户ID' AFTER `ref_id_`;


ALTER TABLE `stos_fin_st`.`wra_sale` 
MODIFY COLUMN `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '-1' COMMENT '租户ID' AFTER `ref_id_`;


ALTER TABLE `stos_fin_st`.`proc_transport` 
ADD COLUMN `audit_status` varchar(2) DEFAULT '2' COMMENT '审核状态  0草稿  1审核中  2已审核' AFTER `update_time_`,
ADD COLUMN `audit_time` datetime(0) COMMENT '审核时间' AFTER `audit_status`,
ADD COLUMN `audit_name` varchar(40) COMMENT '审核人' AFTER `audit_time`;

ALTER TABLE `stos_fin_st`.`proc_transport` 
MODIFY COLUMN `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '-1' COMMENT '租户ID' AFTER `ref_id_`;
ALTER TABLE `stos_fin_st`.`wra_sale_detail` 
MODIFY COLUMN `tenant_id_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '-1' COMMENT '租户ID' AFTER `ref_id_`;
